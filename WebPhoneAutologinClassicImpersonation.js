// ==UserScript==
// @name         WebPhone autologin Classic authentication with impersonation
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Automatic webphone sign-in using impersonation token
// @author       You
// @match        https://phone.enreachvoice.com/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=enreachvoice.com
// @grant        none
// @sandbox      raw
// ==/UserScript==

(async function() {
    'use strict';
    const webrtc = document.querySelector('webrtc-client-ui');

    // Retrieve required items from localstorage
    var userid = localStorage.getItem('autologin_userid');
    var username = localStorage.getItem('autologin_username');

    if (userid != null && username != null) {
        const token = await GetUserToken(userid);
        if (token == null) {
            console.log("Token request failed for user [" + userid + "]");
            return;
        }

        console.log("retrieved token from API:\n" + JSON.stringify(token, undefined, 2));

        var authHeaderObj = {
            value: "Bearer " + token.access_token,
            expiresIn: token.expires_in
        };
        console.log('invoking loginUsingAuthHeader()');
        webrtc.loginUsingAuthHeader(authHeaderObj, userid, username, true);
    }

    else {
        console.log("'autologin_username', 'autologin_userid' must be added to localstorage");
    }

    webrtc.addEventListener('logged-in', function () {
       console.log('Great success! We are now logged in...');
    });

    webrtc.addEventListener('auth-token-expired', function () {
       console.log('Token is about to expire, refreshing...');
       RefreshToken(userid)
        .then(() => console.log('Token refreshed'))
        .catch(error => console.error(error));
    });

})();

async function RefreshToken(userid) {
    const webrtc = document.querySelector('webrtc-client-ui');
    const token = await GetUserToken(userid);
    if (token == null) {
        console.log("Token request failed for user [" + userid + "]");
        return;
    }

    console.log("retrieved token from API:\n" + JSON.stringify(token, undefined, 2));

    var authHeaderObj = {
        value: "Bearer " + token.access_token,
        expiresIn: token.expires_in
    };

    // inject token into webphone
    webrtc.setAuthHeader(authHeaderObj);
}


// This part must be done by secure backend service, which has access to API secret key
// This is just for demo purposes
async function GetUserToken(userid) {

    var apiuser = localStorage.getItem('apiuser_username');
    var apisecret = localStorage.getItem('apiuser_secretkey');

    if (apiuser == null || apisecret == null) {
        console.log("Add 'apiuser_username' and 'apiuser_secretkey' keys to localstorage");
        return;
    }

    var apiUrl = await DiscoverAPI(apiuser);
    if (apiUrl == null) {
        return;
    }

    var url = apiUrl + "/token/" + userid + "/impersonate";
    let response = await fetch(url, {
        method: 'POST',
        headers: {
            "Authorization": "Basic " + btoa(apiuser + ":" + apisecret),
            "Accept": "application/json",
            "X-Json-Serializer": "2"
        }
    });

    if (!response.ok) {
        console.log("Token request failed for user [" + userid + "]");
        return;
    }

    return response.json();
}

// retrieve REST API endpoint for apiuser from discovery service
async function DiscoverAPI(username) {
    var url = "https://discover.enreachvoice.com/api/user/?user=" + username;
    console.log("Discovering user from " + url);
    const response = await fetch(url);

    if (!response.ok) {
        console.log("Discovery failed for user [" + username + "]");
        return;
    }
    const data = await response.json();
    if (data.length > 0) {
        return data[0].apiEndpoint;
    }

}