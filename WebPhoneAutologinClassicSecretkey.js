// ==UserScript==
// @name         WebPhone autologin Classic authentication with secretkey
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Automatic webphone sign-in with secretkey from localstorage
// @author       You
// @match        https://phone.enreachvoice.com/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=enreachvoice.com
// @grant        none
// @sandbox      raw
// ==/UserScript==

(function() {
    'use strict';
    const webrtc = document.querySelector('webrtc-client-ui');

    // Retrieve required items from localstorage
    var username = localStorage.getItem('autologin_username');
    var userid = localStorage.getItem('autologin_userid');
    var apisecret = localStorage.getItem('autologin_apisecret');

    if (username != null && userid != null && apisecret!=null ) {
        // craft HTTP basic auth header, and inject it into webphone
        var authHeader = "Basic " + btoa(username + ":" + apisecret);

        var authHeaderObj = {
            value: authHeader,
            expiresIn: -1 // Basic auth does not expire
        };
        console.log('invoking loginUsingAuthHeader()');
        webrtc.loginUsingAuthHeader(authHeaderObj, userid, username, true);
    }

    else {
        console.log("If you would like to do automagic login, add 'autologin_username', 'autologin_userid' and 'autologin_apisecret' keys to localstorage");
    }

    webrtc.addEventListener('logged-in', function () {
       console.log('Great success! We are now logged in...');
    });

})();

