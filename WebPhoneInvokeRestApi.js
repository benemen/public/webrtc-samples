// ==UserScript==
// @name         WebPhone Invoke REST API
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  WebPhone Invoke REST API on incoming calls
// @author       You
// @match        https://phone.enreachvoice.com/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=enreachvoice.com
// @grant        none
// @sandbox      raw
// ==/UserScript==

(function() {
    'use strict';
    const webrtc = document.querySelector('webrtc-client-ui');   

    webrtc.addEventListener('call-incoming', function () {
       console.log('looks like we have incoming call...');
       getCallInfo();
    });

})();


async function getCallInfo() {

    const webrtc = document.querySelector('webrtc-client-ui');

    // rough retry mechanism, as REST API may not have call info immediately...
    const maxRetries = 2;
    const retryInterval = 500;

    for (let retryCount = 0; retryCount <= maxRetries; retryCount++) {
        try {
            var legs = await webrtc.makeAPICall('GET', '/callmanagement/legs?UserIds=[ME]');
            break;
        }
        catch (error) {
            // If there is an error, log it and wait for the specified interval before retrying
            console.error(`Error on attempt ${retryCount + 1}: ${error.message}`);
            if (retryCount < maxRetries) {
                await new Promise(resolve => setTimeout(resolve, retryInterval));
            } else {
                throw error;
            }
        }
    }

    if (legs == null) {
        return;
    }
    
    // Just print received info to console
    console.log("retrieved call legs from API:\n" + JSON.stringify(legs, undefined, 2));
}

