// ==UserScript==
// @name         WebPhone autologin SSO
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Automatic sign in using SSO
// @author       You
// @match        https://phone.enreachvoice.com/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=enreachvoice.com
// @grant        none
// @sandbox      raw
// ==/UserScript==

(function() {
    'use strict';
    const webrtc = document.querySelector('webrtc-client-ui');

    // Check if we have 'sso_username' set in localstorage. If we have, try to initiate SSO with it
    var sso_username = localStorage.getItem('sso_username');

    if (sso_username != null) {
        // sign in
        console.log("Trying SSO login as " + sso_username);
        webrtc.loginUsingSSO(sso_username);
    }
    else {
        console.log("If you would like to do automagic SSO login, add 'sso_username' key to localstorage");
    }

    // add event listener for logged-in event
    webrtc.addEventListener('logged-in', function () {
        console.log('Great success! We are now logged in...');
     });

})();

