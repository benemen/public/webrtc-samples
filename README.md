# EnreachVoice webphone samples

Collection of Tampermonkey scripts to demonstrate the use of the EnreachVoice webphone javascript API.

The scripts are intended to be used with the [Tampermonkey](https://chromewebstore.google.com/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) browser extension.  

Many scripts utilizeds browser localstorage to store configuration values. You can use [Storage Area Explorer](https://chromewebstore.google.com/detail/storage-area-explorer/ocfjjjjhkpapocigimmppepjgfdecjkb) to view and edit localstorage values.

## Scripts



### Webphone autologin classic authentication with secretkey

[WebPhoneAutologinClassicSecretkey.js](WebPhoneAutologinClassicSecretkey.js)

This script will automatically login to the webphone using classic authentication credentials of the user. Requires that user has classic authentication enabled.

Uses [loginUsingAuthHeader()](https://gitlab.com/benemen/public/webrtc-testclient#loginusingauthheader) JavaScript API method to login to the webphone.

Secretkey, username and userid is set in the localstorage of the browser.

#### Localstorage keys

| Key | Description |
| --- | --- |
| `autologin_apisecret` | The secretkey of the user |
| `autologin_username` | The username of the user |
| `autologin_userid` | The userid of the user |


![Localstorage](img/autologin_legacy.png)

### Webphone autologin classic authentication with impersonation

[WebPhoneAutologinClassicImpersonation.js](WebPhoneAutologinClassicImpersonation.js)

This script will automatically login to the webphone when the page is loaded using impersonation token.

In real produciton use, separate properly secured backend API must be used for retrieveing token for users.

Requires that user has classic authentication enabled.

Uses [loginUsingAuthHeader()](https://gitlab.com/benemen/public/webrtc-testclient#loginusingauthheader) JavaScript API method to login to the webphone and [Impersonation](https://doc.enreachvoice.com/beneapi/#post-token-userid-impersonate) REST API endpoint to retrieve impersonation token.

Secretkey, username and userid is set in the localstorage of the browser.

#### Localstorage keys

| Key | Description |
| --- | --- |
| `autologin_username` | The username of the user to log in|
| `autologin_userid` | The userid of the user to log in |
| `apiuser_username` | The username of the backend apiuser |
| `apiuser_secretkey` | The secretkey of the backend apiuser |


### Webphone autologin with modern authentication

[WebPhoneAutologinModern.js](WebPhoneAutologinModern.js)

This script will automatically login to the webphone when the page is loaded. Requires that user has modern authentication enabled and that the username is set in the localstorage of the browser.

Users [loginUsingSSO()](https://gitlab.com/benemen/public/webrtc-testclient#loginusingauthheader) JavaScript API method to login to the webphone.

#### Localstorage keys

| Key | Description |
| --- | --- |
| `sso_username` | The username of the user |


### Webphone invoke rest call to retrieve additional information for incoming call

[WebPhoneIncomingCallInfo.js](WebPhoneIncomingCallInfo.js)

This script will invoke a rest call to retrieve additional information for incoming call. 
This can be useful if call can be allocated also to mobile-only users, and caller number is set to the ServiceQueue number.

Script use [makeAPICall()](https://gitlab.com/benemen/public/webrtc-testclient#makeapicall) method of webphone JavaScript API to invoke [Call Management REST API endpoint](https://doc.enreachvoice.com/beneapi/#call-management) to retrieve additional information for incoming call.



