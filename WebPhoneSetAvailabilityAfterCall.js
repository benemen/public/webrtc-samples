// ==UserScript==
// @name         WebPhone Set DND Wrapup after call
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  WebPhone Invoke REST API to set user in DND aviailability for 2 minutes everytime an outbound call ends
// @author       You
// @match        https://phone.enreachvoice.com/
// @icon         https://www.google.com/s2/favicons?sz=64&domain=enreachvoice.com
// @grant        none
// @sandbox      raw
// ==/UserScript==

(function() {
  'use strict';
  const webrtc = document.querySelector('webrtc-client-ui');

  webrtc.addEventListener('call-ended', (e) => {
     console.log('looks like call just ended...\n' + JSON.stringify(e.detail, undefined, 2));

     // set useravailability only if call was outbound call
     if (e.detail.callDirection == "outbound") {
         console.log('call was outbound call, set user to wrapup');
         setUserToWrapup();
     }
  });

})();


async function setUserToWrapup() {

  const webrtc = document.querySelector('webrtc-client-ui');

  const now = new Date();
  const wrapupDurationMinutes = 2;
  var endTime = new Date(now.getTime() + wrapupDurationMinutes * 60 * 1000);

  var availability = {
      "EventTypeID": 2, // DND
      "EventSource": "Tampermonkey example",
      "Note": "Wrapup",
      "UserID": webrtc.userId, // userId must be present in the payload
      "EndDate": endTime.toISOString()
  };

  console.log("Sending availability:\n" + JSON.stringify(availability, undefined, 2));
  await webrtc.makeAPICall('POST', '/users/[ME]/availability', availability);
  
}